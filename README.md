# Dorks

Web application that automatically generates dorks (google dorks, ...).

**Live demo :** [https://dorks.xanhacks.xyz](https://dorks.xanhacks.xyz)

## Get started

Install the dependencies :

```bash
$ git clone https://gitlab.com/xanhacks/dorks
$ cd dorks
$ npm install
```

Start a dev server :

```bash
$ npm run dev
```

Build for produciton :

```bash
$ npm run build
$ ls public/
build  favicon.png  global.css  index.html
```

## Features

## Made with

- [Svelte](https://svelte.dev/)